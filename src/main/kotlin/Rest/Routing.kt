package Rest

import ScreenshotMaker.ScreenshotMaker
import io.ktor.application.call
import io.ktor.request.receive
import io.ktor.response.respondFile
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver

fun applyRoute(route: Route) {
    route {
        get("/") {

            val driver: WebDriver = ChromeDriver()

            val s = ScreenshotMaker(driver)


            val f = s.make(
                "https://www.kinopoisk.ru/",
                "body > div.app-container.app-container_app-theme_light > div > div.desktop-layout-with-sidebar__middle > div > div._11SH6Dxak-5SijamlzMDTI.desktop-layout-with-sidebar__container > div > div.desktop-layout-with-sidebar__content > div._2bzmDfg6d5U3EOsV49Su0m.trailer-promo-block._1bdk9_5Gg4bHAPTcANsmR0 > div > div"
            )
            call.respondFile(f)
        }


        post("/") {

            val obj: RequestScreenShoot = call.receive()


            val driver: WebDriver = ChromeDriver()
            val s = ScreenshotMaker(driver)
            val f = s.make(obj.stringUrl, obj.stringTargetCssSelector)
            call.respondFile(f)
        }
    }
}


