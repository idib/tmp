import Rest.applyRoute
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.routing.Routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.text.DateFormat

class Main {}

fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(Routing) {
        applyRoute(this)
    }
}

fun main(args: Array<String>) {
    println("Start")
    val pathToWebDriver = Main::class.java.getResource("webdrive/chromedriver").path
    println(pathToWebDriver)

    System.setProperty("webdriver.chrome.driver", pathToWebDriver);
    embeddedServer(Netty, 8080, watchPaths = listOf("BlogAppKt"), module = Application::module).start()
}