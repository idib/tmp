package ScreenshotMaker

import org.openqa.selenium.*
import java.io.File
import java.util.concurrent.TimeUnit


class ScreenshotMaker(
    private var driver: WebDriver
) {

    fun make(url: String, targetCssSelector: String): File {
        driver.manage()?.timeouts()?.implicitlyWait(10, TimeUnit.SECONDS)
        driver.manage()?.window()?.maximize()

        driver[url]
        val ele = driver.findElement(By.cssSelector(targetCssSelector))
        val screenshot = ele.getScreenshotAs(OutputType.FILE)
        val screenshotLocation = File("/Users/idib/Desktop/$url.png")
        screenshot.copyTo(screenshotLocation, true)
        driver.close()
        return screenshotLocation
    }
}